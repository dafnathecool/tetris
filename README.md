# Tetris with LEDs

* A tetris where the board is a matrix of leds and the input is with a remote control
* option: that the remote control will be an old tetris device that will be reverse engeiered.

1. instructions for a connecting led matrixes to rpi:

https://www.adafruit.com/product/420
-----------------------------------

Detailed reading:
- chaining the the panels is possible only with rpi since not enough ram on arduino
- 1. guide with rpi: https://learn.adafruit.com/adafruit-rgb-matrix-plus-real-time-clock-hat-for-raspberry-pi



https://learn.adafruit.com/16x32-rgb-display-with-raspberry-pi-part-2/you-will-need

https://learn.adafruit.com/32x16-32x32-rgb-led-matrix/overview

https://howchoo.com/pi/raspberry-pi-led-matrix-panel

2. Old tetrises:

https://www.amazon.com/gp/aw/c?ref_=navm_hdr_cart

3. open source tetris that runs on terminal:

https://github.com/k-vernooy/tetris.git

4. open source tetris that run with small graphics with opencv:

https://github.com/VertexC/tetris.git

5. a project of a small matrix:

https://github.com/JonA1961/MAX7219array

This is the matrix:

https://www.amazon.com/HiLetgo-MAX7219-Arduino-Microcontroller-Display/dp/B07FFV537V/ref=as_li_ss_tl?cv_ct_cx=8x8+max7219&dchild=1&keywords=8x8+max7219&pd_rd_i=B07FFV537V&pd_rd_r=9c3b25d9-a29c-43a2-99c2-b28b0e0a4307&pd_rd_w=28171&pd_rd_wg=qiNx5&pf_rd_p=183579a1-f0e6-4556-8e39-8fe08e8f8141&pf_rd_r=ZNSCCD2W1PPM9VNBFS97&psc=1&qid=1590321905&sr=1-1-dd5817a1-1ba7-46c2-8996-f96e7b0f409c&linkCode=sl1&tag=makerguides1-20&linkId=09afb8b70a8424888a9e22828873ec15&language=en_US

6. Create your own matrix:

https://www.instructables.com/Make-Your-Own-LED-Matrix-/

https://maker.pro/arduino/projects/arduino-led-matrix

https://www.instructables.com/LED-Matrix-2/

https://www.raspberrypi.com/news/led-matrix-table/


20.12.2022
----------

troubleshot:
1. on the rpi PUT THE SD CARD WITH THE LINUX
2.

on hamipfal's ifconfig:
```
pi@raspberrypi:~$ ifconfig
eth0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether dc:a6:32:ba:96:d8  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 17  bytes 2168 (2.1 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 17  bytes 2168 (2.1 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.156  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::4441:e9d3:b60:353e  prefixlen 64  scopeid 0x20<link>
        ether dc:a6:32:ba:96:da  txqueuelen 1000  (Ethernet)
        RX packets 251  bytes 57509 (56.1 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 91  bytes 14040 (13.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

pi@raspberrypi:~$

```
To ssh to the pi:
```
ssh pi@192.168.1.156
```

